#define QT_NO_CAST_FROM_ASCII
#include "dataparser.h"
#include <QIODevice>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QStringList>

#include <QDate>

#include "activity.h"


struct ActivityListClearer {
    ActivityListClearer(QList<Activity*>* list) : m_list(list) {}
    void release() { m_list = 0; }
    ~ActivityListClearer() {
        if(m_list) {
            qDeleteAll(*m_list);
            m_list->clear();
        }
    }
    private:
    QList<Activity*>* m_list;
};

DataParser::DataParser(QIODevice* iodevice)
{
    Q_ASSERT(iodevice->isReadable());

    ActivityListClearer clearer(&m_entries); // raii struct to ensure the objectlist is cleared if parsing fails and we do early return

    QByteArray data = iodevice->readAll();

    QJsonParseError error;
    QJsonDocument document = QJsonDocument::fromJson(data, &error);
    if(error.error != QJsonParseError::NoError) {
        m_errorMessage = error.errorString();
        return;
    }

    if(!document.isObject()) {
        m_errorMessage = QStringLiteral("root element is not a object");
        return;
    }
    QJsonObject rootObj = document.object();
    QJsonValue scheduleVal = rootObj.value(QStringLiteral("schedule"));

    if(!scheduleVal.isObject()) {
        m_errorMessage = QStringLiteral("schedule element is not a object");
        return;
    }

    QJsonObject scheduleObj = scheduleVal.toObject();

    QJsonValue conferenceVal = scheduleObj.value(QStringLiteral("conference"));

    if(!conferenceVal.isObject()) {
        m_errorMessage = QStringLiteral("conference element is not a object");
        return;
    }

    QJsonObject conferenceObj = conferenceVal.toObject();


    QJsonValue daysVal = conferenceObj.value(QStringLiteral("days"));

    if(!daysVal.isArray()) {
        m_errorMessage = QStringLiteral("days element is not an array");
        return;
    }

    QJsonArray daysArr = daysVal.toArray();

    Q_FOREACH(QJsonValue dayVal, daysArr) {
        if(!dayVal.isObject()) {
            m_errorMessage = QStringLiteral("a day value is not a object");
            return;
        }
        QJsonObject dayObj = dayVal.toObject();
        QJsonValue dateVal = dayObj.value(QStringLiteral("date"));
        QString dateStr = dateVal.toString();
        QDate dateDate = QDate::fromString(dateStr, Qt::ISODate);

        if(!dateDate.isValid()) {
            m_errorMessage = QStringLiteral("error parsing day");
            return;
        }


        QJsonValue roomsVal = dayObj.value(QStringLiteral("rooms"));
        if(!roomsVal.isObject()) {
            m_errorMessage = QStringLiteral("rooms is not an object");
            return;
        }
        QJsonObject roomsObj = roomsVal.toObject();
        QStringList roomNames = roomsObj.keys();
        Q_FOREACH(QString roomName, roomNames ) {
            QJsonValue roomVal = roomsObj.value(roomName);
            if(!roomVal.isArray()) {
                m_errorMessage = QStringLiteral("a room is not an array: ") + roomName;
                return;
            }
            QJsonArray roomArr = roomVal.toArray();
            Q_FOREACH(QJsonValue talkVal, roomArr) {
                if(!talkVal.isObject()) {
                    m_errorMessage = QStringLiteral("a talk is not an object");
                    return;
                }
                QJsonObject talkObj = talkVal.toObject();
                Activity* activity = new Activity();
                m_entries << activity;
                activity->setTitle(talkObj.value(QStringLiteral("title")).toString());

                QTime startTime = QTime::fromString(talkObj.value(QStringLiteral("start")).toString());
                if(!startTime.isValid()) {
                    m_errorMessage = QStringLiteral("parsing time failed");
                    return;
                }
                QTime duration = QTime::fromString(talkObj.value(QStringLiteral("duration")).toString());
                if(!duration.isValid()) {
                    m_errorMessage = QStringLiteral("parsing duration failed");
                    return;
                }
                QTime endTime = startTime.addMSecs(duration.msecsSinceStartOfDay());
                if(!duration.isValid() || endTime < startTime) {
                    m_errorMessage = QStringLiteral("creating endtime failed");
                    return;
                }

                activity->setStartTime(QDateTime(dateDate, startTime));
                activity->setEndTime(QDateTime(dateDate, endTime));

                activity->setTrack(talkObj.value(QStringLiteral("track")).toString());
                activity->setRoom(talkObj.value(QStringLiteral("room")).toString());
            }

        }



    }

    clearer.release(); // everything parsed succesful. Let's keep the list content.

}

QString DataParser::errorMessage() const
{
    return m_errorMessage;
}

bool DataParser::isValid()
{
    return m_errorMessage.isEmpty();
}

QList<Activity*> DataParser::takeEntries()
{
    QList<Activity*> objects = m_entries;
    m_entries.clear();
    m_errorMessage = QStringLiteral("Take entries has been called");
    return objects;
}

DataParser::~DataParser()
{
    qDeleteAll(m_entries);
}
