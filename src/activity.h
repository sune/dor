#ifndef ACTIVITY_H
#define ACTIVITY_H

#include <QObject>
#include <QString>
#include <QDateTime>

class Activity : public QObject {
    Q_OBJECT
    Q_PROPERTY(Activity::Type type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(QString room READ room WRITE setRoom NOTIFY roomChanged)
    Q_PROPERTY(QString track READ track WRITE setTrack NOTIFY trackChanged)
    Q_PROPERTY(QDateTime startTime READ startTime WRITE setStartTime NOTIFY startTimeChanged)
    Q_PROPERTY(QDateTime endTime READ endTime WRITE setEndTime NOTIFY endTimeChanged)
    public:
        Activity(QObject* parent = 0);

        enum Type {
            Unknown,
            Talk,
            LighteningTalk,
            Break
        };
        Q_ENUMS(Type)
        Type type() const;
        void setType(Type newType);
        QString title() const;
        void setTitle(const QString& newTitle);
        QString room() const;
        void setRoom(const QString& newRoom);
        QString track() const;
        void setTrack(const QString& newTrack);
        QDateTime startTime() const;
        void setStartTime(const QDateTime& newStart);
        QDateTime endTime() const;
        void setEndTime(const QDateTime& newEnd);

    Q_SIGNALS:
        void typeChanged();
        void titleChanged();
        void startTimeChanged();
        void endTimeChanged();
        void trackChanged();
        void roomChanged();
    private:
        QString m_title;
        QString m_track;
        QString m_room;
        QDateTime m_startTime;
        QDateTime m_endTime;
        Type m_type;
};

#endif
