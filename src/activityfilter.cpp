#include "activityfilter.h"
#include "activity.h"

#include <QTimer>

ActivityFilter::ActivityFilter(QObject* parent) : QObject(parent), m_date(QDate::currentDate()), m_updateCurrentActivityTimer(new QTimer(this)), m_currentActivity(0)
{
    m_updateCurrentActivityTimer->setSingleShot(true);
    (void)connect(m_updateCurrentActivityTimer, &QTimer::timeout, this, &ActivityFilter::checkAndUpdateCurrentActivity);
}

void ActivityFilter::setAllActivities(const QList<Activity*>& activities)
{
    QList<Activity*> oldActivities = m_allActivities;
    m_allActivities = activities;
    updateFiltered();
    qDeleteAll(oldActivities);
}

QVariant ActivityFilter::activities() const
{
    return QVariant::fromValue(m_filteredActivities);
}

QString ActivityFilter::room() const
{
    return m_room;
}

void ActivityFilter::setRoom(const QString& room)
{
    if(m_room != room) {
        m_room = room;
        updateFiltered();
        roomChanged();
    }
}

QDate ActivityFilter::date() const
{
    return m_date;
}

void ActivityFilter::setDate(const QDate& date)
{
    if(date != m_date) {
        m_date = date;
        updateFiltered();
        dateChanged();
    }
}

QObject*ActivityFilter::currentActivity() const
{
    return m_currentActivity;
}

ActivityFilter::~ActivityFilter()
{
    qDeleteAll(m_allActivities);
    m_allActivities.clear();
    m_filteredActivities.clear();
}

void ActivityFilter::checkAndUpdateCurrentActivity()
{
    Activity* newCurrent = 0;
    Q_FOREACH(QObject* activityObject, m_filteredActivities) {
        Activity* activity = static_cast<Activity*>(activityObject);
        if(activity->endTime().time() >= QTime::currentTime()) {
            newCurrent = activity;
            break;
        }
    }
    if(m_currentActivity != newCurrent) {
        m_currentActivity = newCurrent;
        currentActivityChanged();
    }
    if(m_currentActivity) {
        m_updateCurrentActivityTimer->start(qMax((m_currentActivity->endTime().time().msecsSinceStartOfDay() - QTime::currentTime().msecsSinceStartOfDay())/2,1000));
    }
}

void ActivityFilter::updateFiltered()
{
    m_filteredActivities.clear();
    Q_FOREACH(Activity* activity, m_allActivities) {
        if(activity->room() == m_room || m_room.isEmpty()) {
            if(activity->startTime().date() == m_date) {
                m_filteredActivities << activity;
            }
        }
    }
    checkAndUpdateCurrentActivity();
    activitiesChanged();
}
