#ifndef DATAPARSER_H
#define DATAPARSER_H

#include <QList>
#include <QString>

class QIODevice;
class Activity;

class DataParser {
    public:
        DataParser(QIODevice* iodevice);
        QString errorMessage() const;
        bool isValid();
        /**
         * @brief takeEntries takes the parsed entries. Ownership of the list content is responsibility of the caller.
         * After this function is called, this object's state is undefined.
         *
         * Note that if \ref isValid returns false, this object might still have content in it, but is only the result
         * of a partial parse
         * @return the parsed objects
         */
        QList<Activity*> takeEntries();
        ~DataParser();
    private:
        QList<Activity*> m_entries;
        QString m_errorMessage;
};

#endif // DATAPARSER_H
