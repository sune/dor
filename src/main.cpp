#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <QCommandLineParser>
#include <QCommandLineOption>

#include <QFile>

#include "activity.h"
#include "activityfilter.h"

#include "dataparser.h"

#include <QDebug>

#include <iostream>

Q_NORETURN void error(QString message) {
    std::cout << "ERROR: " << qPrintable(message) << std::endl;
    ::exit(1);
}

int main(int argc, char** argv) {
    QGuiApplication app(argc, argv);
    app.setOrganizationName(QStringLiteral("KDE"));
    app.setOrganizationDomain(QStringLiteral("kde.org"));

    QQmlApplicationEngine engine;

    QCommandLineParser cmdline;
    QCommandLineOption filenameOption(QStringLiteral("schedule"),QStringLiteral("filename for schedule"), QStringLiteral("filename"));
    QCommandLineOption dateOption(QStringLiteral("date"), QStringLiteral("Date to use if not current date"), QStringLiteral("YYYY-mm-dd"));
    QCommandLineOption roomOption(QStringLiteral("room"), QStringLiteral("Room to filter for"), QStringLiteral("room"));
    cmdline.addOption(filenameOption);
    cmdline.addOption(dateOption);
    cmdline.addOption(roomOption);
    cmdline.addHelpOption();

    cmdline.process(app);

    if(!cmdline.isSet(filenameOption)) {
        error("filename not set");
    }

    ActivityFilter* activityFilter = new ActivityFilter(&engine);

    if(cmdline.isSet(dateOption)) {
        QDate date = QDate::fromString(cmdline.value(dateOption), Qt::ISODate);
        if(!date.isValid()) {
            error(QStringLiteral("failed to parse date"));
        }
        activityFilter->setDate(date);
    }

    if(cmdline.isSet(roomOption)) {
        activityFilter->setRoom(cmdline.value(roomOption));
    }

    QString filename = cmdline.value(filenameOption);

    if(!QFile::exists(filename)) {
        error(QStringLiteral("file doesn't exist:%1 ").arg(filename));
    }

    QFile f(filename);
    if(!f.open(QIODevice::ReadOnly)) {
        error(QStringLiteral("failed to open file").arg(f.errorString()));
    }

    DataParser parser(&f);
    if(!parser.isValid()) {
        error(parser.errorMessage());
    }

    activityFilter->setAllActivities(parser.takeEntries());

    engine.rootContext()->setContextProperty("events", activityFilter);
    engine.load(QUrl("qrc:///qml/mainwindow.qml"));

    return app.exec();
}
