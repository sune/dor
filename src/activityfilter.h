#ifndef ACTIVITYFILTER_H
#define ACTIVITYFILTER_H

#include <QObject>
#include <QVariant>
#include <QDate>

class Activity;
class QTimer;

class ActivityFilter : public QObject {
    Q_OBJECT
    Q_PROPERTY(QVariant activities READ activities NOTIFY activitiesChanged)
    Q_PROPERTY(QString room READ room WRITE setRoom NOTIFY roomChanged)
    Q_PROPERTY(QDate date READ date WRITE setDate NOTIFY dateChanged)
    Q_PROPERTY(QObject* currentActivity READ currentActivity NOTIFY currentActivityChanged)
    public:
        ActivityFilter(QObject* parent = 0);
        /**
         * @brief setAllActivities
         * this class takes ownership of the activity objects
         * @param activities
         */
        void setAllActivities(const QList<Activity*>& activities);
        QVariant activities() const;

        QString room() const;
        /**
         * @brief setRoom
         * Filters the activities to a specific room (empty string is default and means all rooms)
         * @param room
         */
        void setRoom(const QString& room);
        QDate date() const;
        /**
         * @brief setDate
         * Filters the activities to a specific date (or current date if not set)
         * @param date
         */
        void setDate(const QDate& date);

        QObject* currentActivity() const;


        ~ActivityFilter();
    Q_SIGNALS:
        void activitiesChanged();
        void roomChanged();
        void dateChanged();
        void currentActivityChanged();
    private Q_SLOTS:
        void checkAndUpdateCurrentActivity();
    private:
        QObjectList m_filteredActivities;
        QList<Activity*> m_allActivities;
        QString m_room;
        QDate m_date;
        QTimer* m_updateCurrentActivityTimer;
        Activity* m_currentActivity;
        void updateFiltered();
};

#endif //ACTIVITYFILTER_H
