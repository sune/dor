import QtQuick 2.0

ListModel {
    ListElement {
        name: "Room 1"
        action: "Room 1"
        type: "setRoom"
    }
    ListElement {
        name: "Room 2"
        action: "Room 2"
        type: "setRoom"
    }
    ListElement {
        name: "Exit"
        action: "exit"
        type: "app"
    }
}
