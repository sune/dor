import QtQuick 2.0

import "helpers.js" as Helpers

RootWindow {
    width: 320
    height: 480
    title: events.room + ": " + (events.currentActivity ? events.currentActivity.title : "No event")
}
