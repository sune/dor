
function toggleMenu() {
    if (!root.menuActive)
        root.menuActive = true;
    else
        root.menuActive = false;
}

function startAction(name, type) {
    if (type === "app") {
        if (name === "exit") Qt.quit();
    } else if (type === "page") {
        main.source = "qrc:/" + name + ".qml";
    } else if (type === "setRoom") {
        events.room = name;
    }
    toggleMenu();
}

function secondsOfDay(date) {
    return date.getHours() * 3600 + date.getMinutes() * 60 + date.getSeconds();
}
