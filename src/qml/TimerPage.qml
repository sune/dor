import QtQuick 2.0

import "helpers.js" as Helpers

Rectangle {
    color: "black"

    Timer {
        running: true;
        repeat: true
        onTriggered: {
            if(!events.currentActivity) {
                time.text = "No event"
                return
            }

            var currentSeconds = Helpers.secondsOfDay(new Date())
            var countDownToStart = false
            var seconds;

            var currentSecondsStartTime = Helpers.secondsOfDay(events.currentActivity.startTime)
            if(currentSecondsStartTime > currentSeconds) {
                seconds = currentSecondsStartTime - currentSeconds
                countDownToStart = true;
            } else {
                var currentSecondsEndTime = Helpers.secondsOfDay(events.currentActivity.endTime)
                seconds = currentSecondsEndTime - currentSeconds
            }

            //var seconds = Math.floor(currentTimer/1000);
            var strMinutes = "" + Math.floor(seconds/60);
            var strSeconds = "";
            if (seconds%60 < 10) strSeconds = "0";
            strSeconds += seconds%60;
            time.text = strMinutes + ":" + strSeconds;

            var color = "white";
            if ( (seconds < 300) && (seconds >= 120) ) {
                color = "orange";
            } else if (seconds < 120) {
                color = "red";
            }
            if(countDownToStart) {
                color = "green"
            }

            time.color = color;
        }
    }

    Text {
        id: time
        anchors.centerIn: parent
        color: "white"
        font.pixelSize: parent.width*0.25
        font.family: "Open Sans,Arial,Helvetica,sans-serif"
    }

}
