import QtQuick 2.0
import QtQuick.Window 2.0

Window {
    property string title: "Beere Components"
    property bool menuActive: false

    id: root
    visible: true
    color: "black"

    Loader {
        id: main
        anchors.top: titlebar.bottom
        anchors.bottom: parent.bottom
        width: parent.width
        source: "qrc:/qml/TimerPage.qml"
        visible: !menuActive
    }

    TitleBar {
        id: titlebar
        anchors.top: parent.top
        width: parent.width
        height: 0.12*parent.height

    }

    Menu {
        id: menu
        anchors.top: titlebar.bottom
        anchors.bottom: parent.bottom
        width: parent.width
        visible: menuActive

    }


}
