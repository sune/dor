#include "activity.h"



Activity::Activity(QObject* parent) : QObject(parent), m_type(Unknown)
{

}

Activity::Type Activity::type() const
{
    return m_type;
}

void Activity::setType(Activity::Type newType)
{
    if(m_type != newType) {
        m_type = newType;
        emit typeChanged();
    }
}

QString Activity::title() const
{
    return m_title;
}

void Activity::setTitle(const QString& newTitle)
{
    if(m_title != newTitle) {
        m_title = newTitle;
        emit titleChanged();
    }
}

QString Activity::room() const
{
    return m_room;
}

void Activity::setRoom(const QString& newRoom)
{
    if(m_room != newRoom) {
        m_room = newRoom;
        emit roomChanged();
    }
}

QString Activity::track() const
{
    return m_track;
}

void Activity::setTrack(const QString& newTrack)
{
    if(m_track != newTrack) {
        m_track = newTrack;
        emit trackChanged();
    }
}

QDateTime Activity::startTime() const
{
    return m_startTime;
}

void Activity::setStartTime(const QDateTime& newStart)
{
    if(m_startTime != newStart) {
        m_startTime = newStart;
        emit startTimeChanged();
    }
}

QDateTime Activity::endTime() const
{
    return m_endTime;
}

void Activity::setEndTime(const QDateTime& newEnd)
{
    if(m_endTime != newEnd) {
        m_endTime = newEnd;
        emit endTimeChanged();
    }
}


